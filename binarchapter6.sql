-- -------------------------------------------------------------
-- TablePlus 2.10(268)
--
-- https://tableplus.com/
--
-- Database: binarchapter6
-- Generation Time: 2021-02-07 08:45:40.6810
-- -------------------------------------------------------------


-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."SequelizeMeta" (
    "name" varchar(255) NOT NULL,
    PRIMARY KEY ("name")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "UserAdmins_id_seq";

-- Table Definition
CREATE TABLE "public"."UserAdmins" (
    "id" int4 NOT NULL DEFAULT nextval('"UserAdmins_id_seq"'::regclass),
    "username" varchar(255),
    "password" varchar(255),
    "email" varchar(255) NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "UserGameBios_id_seq";

-- Table Definition
CREATE TABLE "public"."UserGameBios" (
    "id" int4 NOT NULL DEFAULT nextval('"UserGameBios_id_seq"'::regclass),
    "nama" varchar(255),
    "umur" varchar(255),
    "hobi" varchar(255),
    "user_id" int4,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "UserGameHistories_id_seq";

-- Table Definition
CREATE TABLE "public"."UserGameHistories" (
    "id" int4 NOT NULL DEFAULT nextval('"UserGameHistories_id_seq"'::regclass),
    "name_player" varchar(255),
    "status" varchar(255),
    "score" varchar(255),
    "playDate" timestamptz,
    "user_id" int4,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "UserGames_id_seq";

-- Table Definition
CREATE TABLE "public"."UserGames" (
    "id" int4 NOT NULL DEFAULT nextval('"UserGames_id_seq"'::regclass),
    "username" varchar(255) NOT NULL,
    "email" varchar(255) NOT NULL,
    "password" varchar(255) NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    PRIMARY KEY ("id")
);

INSERT INTO "public"."SequelizeMeta" ("name") VALUES ('20210202124557-users.js'),
('20210202124617-user_games.js'),
('20210202124634-user_game_bios.js'),
('20210202124654-user_game_histories.js');

INSERT INTO "public"."UserAdmins" ("id", "username", "password", "email", "createdAt", "updatedAt") VALUES ('1', 'yoanes', '$2b$08$dL7Riy6ZRAo7Fiwu4bBzKeA98av.sKCYmyNn2sumf.KfLz6fPOR0O', 'yoanesrn@gmail.com', '2021-02-06 21:37:21.816+07', '2021-02-06 21:37:21.816+07');

INSERT INTO "public"."UserGameBios" ("id", "nama", "umur", "hobi", "user_id", "createdAt", "updatedAt") VALUES ('1', 'ss123', '18', 'makan123', NULL, '2021-02-06 21:37:42.904+07', '2021-02-06 21:47:32.472+07'),
('2', 'ss', '15', 'boboks', NULL, '2021-02-06 21:39:43.223+07', '2021-02-06 21:39:43.223+07');

INSERT INTO "public"."UserGameHistories" ("id", "name_player", "status", "score", "playDate", "user_id", "createdAt", "updatedAt") VALUES ('26', 'vincent', NULL, '0', '2021-02-07 08:29:29.252+07', NULL, '2021-02-07 08:29:29.253+07', '2021-02-07 08:29:29.253+07'),
('27', 'vincent', NULL, '0', '2021-02-07 08:29:44.392+07', NULL, '2021-02-07 08:29:44.392+07', '2021-02-07 08:29:44.392+07'),
('28', 'vincent', NULL, '0', '2021-02-07 08:29:45.67+07', NULL, '2021-02-07 08:29:45.67+07', '2021-02-07 08:29:45.67+07'),
('29', 'vincent', NULL, '0', '2021-02-07 08:29:46.469+07', NULL, '2021-02-07 08:29:46.47+07', '2021-02-07 08:29:46.47+07'),
('30', 'vincent', NULL, '0', '2021-02-07 08:29:47.139+07', NULL, '2021-02-07 08:29:47.139+07', '2021-02-07 08:29:47.139+07'),
('31', 'vincent', NULL, '0', '2021-02-07 08:29:47.754+07', NULL, '2021-02-07 08:29:47.754+07', '2021-02-07 08:29:47.754+07'),
('32', 'vincent', NULL, '0', '2021-02-07 08:29:48.744+07', NULL, '2021-02-07 08:29:48.744+07', '2021-02-07 08:29:48.744+07'),
('33', 'vincent', NULL, '1', '2021-02-07 08:29:49.566+07', NULL, '2021-02-07 08:29:49.566+07', '2021-02-07 08:29:49.566+07'),
('34', 'vincent', NULL, '1', '2021-02-07 08:29:50.204+07', NULL, '2021-02-07 08:29:50.204+07', '2021-02-07 08:29:50.204+07'),
('35', 'vincent', NULL, '2', '2021-02-07 08:30:04.261+07', NULL, '2021-02-07 08:30:04.261+07', '2021-02-07 08:30:04.261+07'),
('36', 'vincent', NULL, '2', '2021-02-07 08:30:05.291+07', NULL, '2021-02-07 08:30:05.292+07', '2021-02-07 08:30:05.292+07'),
('37', 'vincent', NULL, '3', '2021-02-07 08:30:22.099+07', NULL, '2021-02-07 08:30:22.099+07', '2021-02-07 08:30:22.099+07'),
('38', 'vincent', NULL, '4', '2021-02-07 08:30:33.252+07', NULL, '2021-02-07 08:30:33.252+07', '2021-02-07 08:30:33.252+07'),
('39', 'vincent', NULL, '0', '2021-02-07 08:37:52.966+07', NULL, '2021-02-07 08:37:52.967+07', '2021-02-07 08:37:52.967+07'),
('40', 'vincent', NULL, '1', '2021-02-07 08:37:54.319+07', NULL, '2021-02-07 08:37:54.319+07', '2021-02-07 08:37:54.319+07');

INSERT INTO "public"."UserGames" ("id", "username", "email", "password", "createdAt", "updatedAt") VALUES ('2', 'yas123', 'yas@gmail.com', '$2b$10$ggRNZyPim3D/LaMxbF6deOtJD6Lh7jHJxaKx00dveLEytOXC01dqy', '2021-02-06 21:46:49.948+07', '2021-02-06 21:46:59.715+07'),
('3', 'yas', 'yas@gmail.com', '$2b$10$9ZU2mnP05dimv6lormrLjepuCKBPso5.oxIxGupGgDXSidYFM4OfO', '2021-02-06 21:51:04.776+07', '2021-02-06 21:51:04.776+07'),
('4', 'vincent', 'vincentGE@gmail.com', '$2b$10$T10XXILzIibFrIPzVmnDZeNBubfuhlbFwBkW7cRmLCI6BvTIG1aCa', '2021-02-07 08:21:36.105+07', '2021-02-07 08:21:36.105+07');

ALTER TABLE "public"."UserGameBios" ADD FOREIGN KEY ("user_id") REFERENCES "public"."UserGames"("id");
ALTER TABLE "public"."UserGameHistories" ADD FOREIGN KEY ("user_id") REFERENCES "public"."UserGames"("id");
