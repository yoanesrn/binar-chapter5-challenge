const express = require('express')
const app = express()
const router = express.Router();
let models = require('../models')
const sessionChecker = require('../sessionChecker/sessionChecker')



router.get('/dashboard/user-game', async (req, res) =>  {
    
    const allUserGame = await findAllUserGame();
   
    res.render('user-game/user-game',{userGame: allUserGame });
});

function findAllUserGame() {
    try {
        return models.UserGame.findAll({});   
    } catch (error) {
        console.log(error);
    }
}


router.get('/dashboard/user-game/add', sessionChecker, (req, res) => {
    res.render('user-game/add');
});

router.post('/dashboard/user-game/add', sessionChecker, (req, res) => {
    models.UserGame.create({
        username: req.body.username,
        password: req.body.password,
        email: req.body.email
    })
    .then(user => {
        res.redirect('/dashboard/user-game');

    })
    .catch(error => {
        res.redirect('/dashboard/user-game/add');
    });
});

router.get('/dashboard/user-game/edit/:id',sessionChecker, (req, res) => {
    models.UserGame.findByPk(req.params.id).then((userGame)=>{
        res.render('user-game/edit',{userGame});
    });
});

router.put('/dashboard/user-game/edit/:id', sessionChecker,(req, res) => {
    const {username, email} = req.body;
    models.UserGame.update(req.body, {
    where:{
      id:req.params.id
    }
  }).then((user) => {
    res.redirect('/dashboard/user-game')
  })

});
 

router.delete('/dashboard/user-game/delete/:id', sessionChecker,(req, res) => {
    models.UserGame.destroy({
      where:{
        id:req.params.id
      }
    }).then((user) => {  
        res.send({ msg: "Success" });
    })
});



module.exports = router;